NAME = envayro-slave
MCU = cortex-m3
TOOLCHAIN = arm-none-eabi

CC = $(TOOLCHAIN)-gcc
AS = $(TOOLCHAIN)-as
OBJCOPY = $(TOOLCHAIN)-objcopy

# ld
LDFILE = link.ld

# src
SRC += ./main.c

SRC += ./sys.c
SRC += ./crc16.c
SRC += ./device.c
SRC += ./rtc.c
SRC += ./eeprom.c

SRC += ./cmsis_boot/system_stm32f10x.c
SRC += ./stm_lib/src/stm32f10x_gpio.c
SRC += ./stm_lib/src/stm32f10x_rcc.c
SRC += ./stm_lib/src/stm32f10x_pwr.c
SRC += ./stm_lib/src/misc.c
SRC += ./stm_lib/src/stm32f10x_usart.c
SRC += ./stm_lib/src/stm32f10x_tim.c
SRC += ./stm_lib/src/stm32f10x_exti.c
SRC += ./stm_lib/src/stm32f10x_i2c.c
SRC += ./stm_lib/src/stm32f10x_adc.c
SRC += ./stm_lib/src/stm32f10x_crc.c

OBJS = $(SRC:.c=.o)

#inc
INC += .
INC += ./cmsis
INC += ./cmsis_boot
INC += ./stm_lib/inc
INC += ./syscalls

CINC = $(INC:%=-I%)

#asm src
ASRC += ./cmsis_boot/startup_stm32f105xc.s

OBJS += $(ASRC:.s=.o)

CFLAGS = -mcpu=$(MCU) -mthumb -g -Wall -O2 -ffunction-sections -DSTM32F10X_CL=1 -DUSE_STDPERIPH_DRIVER=1 -DSTM32F105xC=1 -DDBG=1
LDFLAGS = -mcpu=$(MCU) --specs=nosys.specs -mthumb -O2 -gc-section -T$(LDFILE) -Wl,-Map=$(NAME).map,--cref,--no-warn-mismatc -nostartfiles
ASFLAGS = -mcpu=$(MCU) -mthumb -g -Wall -gdwarf-2

default: $(OBJS) $(NAME).elf $(NAME).hex $(NAME).bin
all: $(OBJS) $(NAME).elf $(NAME).hex $(NAME).bin flash

.c.o:
	$(CC) $(CFLAGS) $(CINC) -c $< -o $@

.s.o:
	$(CC) $(ASFLAGS) $(CINC) -c $< -o $@

$(NAME).elf: $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@

$(NAME).hex: $(NAME).elf
	$(OBJCOPY) -O ihex $^ $@

$(NAME).bin: $(NAME).elf
	$(OBJCOPY) -O binary -S $^ $@

flash:
	openocd -f openocd.cfg
	
rst:
	openocd -f openocd_rst.cfg

clean:
	rm -f $(OBJS)
	rm -f $(NAME).hex
	rm -f $(NAME).elf
	rm -f $(NAME).bin
	rm -f $(NAME).map
