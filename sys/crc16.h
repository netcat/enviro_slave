#ifndef CRC16_H
#define CRC16_H

#include "sys.h"

uint16_t calc_crc16(uint8_t *data, uint8_t len);

#endif
