#include "device.h"
#include "string.h"
#include "sys.h"
#include "eeprom.h"
#include "rtc.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

device_config_t device_config;
const device_config_t device_config_default =
{ { DEFAULT_ADDR }, { DEFAULT_PWD }, (uint8_t)BAUD_9600, (uint8_t)OUT_AUTOMATIC,
      (uint8_t)DEVICE_SESSION_MIN_DEFAULT, (uint8_t)3, (uint8_t)DEBUG_DISABLE, (uint8_t)DEVICE_CTRL_WAIT_SEC_DEFAULT };

device_rtc_t sysclock;
const device_rtc_t sysclock_default = { { 12, 0, 0 }, { 1, 1, 0 } };

device_rule_t rules[DEVICE_RULES_NUM];
device_rule_t rules_default[] = {
  { 8, 0, 1, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 },
  { 0, 0, 0, 0 }
};

version_t sw_version = { 1, 3 };

volatile unsigned int system_check_rules_flag = 0;
volatile unsigned int system_sync_clock_flag = 1;
volatile unsigned int system_output_timer_exp_flag = 0;

volatile unsigned int system_output_timer = 0;
volatile unsigned int session_timer = 0;
volatile unsigned int system_sync_clock_timer = 0;

uint8_t last_output_status = 0;

const interface_obj_t objs[] = {
  { CMD_OPEN, OBJ_WR, DEVICE_PWD_LEN },
  { CMD_CLOSE, OBJ_WR, 0 },
  { CMD_SERIAL_NUMBER, OBJ_RW, DEVICE_SERIAL_NUMBER_LEN },
  { CMD_CLOCK, OBJ_RW, sizeof(device_rtc_t) },
  { CMD_BAUDRATE, OBJ_RW, 1 },
  { CMD_SOFTWARE_VERSION, OBJ_RD, sizeof(sw_version) },
  { CMD_RESET_CONFIG, OBJ_WR, 0 },
  { CMD_PWD, OBJ_RW, DEVICE_PWD_LEN },
  { CMD_RULES, OBJ_RW, sizeof(rules) },
  { CMD_OUTPUT_CONFIG, OBJ_RW, 1 },
  { CMD_OUTPUT_CTRL, OBJ_RW, 1 },
  { CMD_MCU_TEMPERATURE, OBJ_RD, 0 },
  { CMD_BATTARY_VOLTAGE, OBJ_RD, 0 },
  { CMD_ACTIVE_RULE, OBJ_RD, 0 },

  { CMD_DUBUG_MODE, OBJ_RW, 1 },
  { CMD_CTRL_WAIT_SEC, OBJ_RW, 1 },
  { CMD_SESSION_TIME_MIN, OBJ_RW, 1 },
  { CMD_SERIAL_NUMBER_TEXT, OBJ_RD, 0 },
  { CMD_SELFTEST, OBJ_WR, 0 },
  };

#define DEVICE_NACK(cmd, status) { if(session_timer) device_send_status(cmd, status); return; }

#ifdef DBG
void syslog(const char *fmt, ...)
{
  if(!device_config.debug_mode) return;

  va_list argptr;
  int len = 1;
  char str[100];

  memset(&str, 0, sizeof(str));
  va_start(argptr, fmt);
  len = vsprintf(str, fmt, argptr);
  va_end(argptr);

  if_pkg_t pkg = { (uint8_t*)str, len + 1 };
  device_send_data(CMD_LOG_MSG, &pkg);

  wait_ms(DEVICE_TIMOUT);
}
#endif

void device_receive(if_pkg_t *pkg)
{
//  syslog_obj();

  uint8_t *data = pkg->data;
  // check minimum size
  if(pkg->size < DEVICE_PKG_SERVICE_SIZE)
  {
    syslog("pkg size: %d, but min allowed %d", pkg->size, DEVICE_PKG_SERVICE_SIZE);
    return;
  }
//  syslog("pkg size: %d bytes, size ok! ", pkg->size);

  // check serial number
  if(ncmp(data, device_config.serial_number, sizeof(device_config.serial_number)))
  {
//    syslog("s/n: ignored!", pkg->size);
    return;
  }
//  syslog("s/n: ok!", pkg->size);
  data += sizeof(device_config.serial_number);

  // get access type
  if_access_type_e access_type = (*data & ((uint8_t)0x80)) ? OBJ_WR : OBJ_RD;

  // check cmd
  uint8_t cmd = *data;
  data += DEVICE_ACCESS_COMMAND_LEN;
  int i;
  for(i = 0; i < SIZE_OF_ARRAY(objs); i++)
  {
    if((cmd & 0x7f) == (uint8_t)objs[i].command) break;
  }
  if(i >= SIZE_OF_ARRAY(objs))
  {
    syslog("invalid command!");
    DEVICE_NACK(cmd, ST_BAD_CMD);
  }
//  syslog("command ok: %s", if_cmd_e_str[i]);

  // check access type
  uint8_t t = (uint8_t)objs[i].access_type;
  if(t & access_type)
  {
//    syslog("access type: %s, allowed %s", access_type_str[access_type], access_type_str[t]);
  }
  else
  {
    syslog("access denied: request %s, but allowed only %s", access_type_str[access_type], access_type_str[objs[i].access_type]);
    DEVICE_NACK(cmd, ST_BAD_ACCESS_TYPE);
  }

  // check crc
  uint16_t crc = calc_crc16(pkg->data, pkg->size - DEVICE_CRC_LEN);
  if(ncmp(&pkg->data[pkg->size - DEVICE_CRC_LEN], (uint8_t*)&crc, DEVICE_CRC_LEN))
  {
    syslog("crc error");
    return;
  }
//  syslog("crc: ok");

  // check data size
  int data_size = pkg->size - DEVICE_PKG_SERVICE_SIZE;
  if(access_type == OBJ_WR)
  {
    if(objs[i].data_size != data_size)
    {
      syslog("bad data size for write!");
      DEVICE_NACK(cmd, ST_BAD_DATA_SIZE);
    }
  }
  else
  {
    if(data_size > 0)
    {
      syslog("bad data size for read (must be 0, really: %d)!", data_size);
      DEVICE_NACK(cmd, ST_BAD_DATA_SIZE);
    }
  }
//  syslog("pkg size correct!");

  // ret
  pkg->data = data;
  pkg->size = objs[i].data_size;
  device_pkg_handler(cmd, pkg, access_type);
}

void device_send_data(if_cmd_e cmd, if_pkg_t *pkg)
{
//  syslog_obj();

  uint8_t service_data[DEVICE_PKG_ANS_SIZE + pkg->size];
  if_pkg_t net_pkg = { service_data, sizeof(service_data) };

  uint8_t *data = net_pkg.data;
  // addr
  memcpy(data, device_config.serial_number, DEVICE_SERIAL_NUMBER_LEN);
  data += DEVICE_SERIAL_NUMBER_LEN;
  // status
  *data++ = (uint8_t)ST_OK;
  // cmd
  *data++ = (uint8_t)cmd;
  // data size
  *data++ = (uint8_t)pkg->size;
  // data
  memcpy(data, pkg->data, pkg->size);
  data += pkg->size;
  // crc
  uint16_t crc = calc_crc16(net_pkg.data, net_pkg.size - DEVICE_CRC_LEN);
  memcpy(data, (uint8_t*)&crc, DEVICE_CRC_LEN);

  uart2_tx(net_pkg.data, net_pkg.size);
}

void device_send_status(if_cmd_e cmd, if_status_e status)
{
//  syslog_obj();

  uint8_t service_data[DEVICE_PKG_ANS_SIZE];
  if_pkg_t net_pkg = { service_data, sizeof(service_data) };

  uint8_t *data = net_pkg.data;
  // addr
  memcpy(data, device_config.serial_number, sizeof(device_config.serial_number));
  data += DEVICE_SERIAL_NUMBER_LEN;
  // status
  *data++ = (uint8_t)status;
  // cmd
  *data++ = (uint8_t)cmd;
  // data size
  *data++ = 0;
  // crc
  uint16_t crc = calc_crc16(net_pkg.data, net_pkg.size - DEVICE_CRC_LEN);
  memcpy(data, (uint8_t*)&crc, DEVICE_CRC_LEN);

  uart2_tx(net_pkg.data, DEVICE_PKG_ANS_SIZE);
}

void device_default_cfg()
{
  syslog_obj();

  device_config = device_config_default;
  sysclock = sysclock_default;

  int i;
  for(i = 0; i < SIZE_OF_ARRAY(rules_default); i++)
    rules[i] = rules_default[i];

  device_save_clock();
  device_save_cfg();
}

void device_systimer_tick()
{
  sysclock.time.sec++;
  if(sysclock.time.sec >= 60)
  {
    sysclock.time.sec = 0;
    system_check_rules_flag = 1;

    sysclock.time.min++;
    if(sysclock.time.min >= 60)
    {
      sysclock.time.min = 0;

      sysclock.time.hour++;
      if(sysclock.time.hour >= 24)
      {
        sysclock.time.hour = 0;

        sysclock.date.day_of_week++;
        if(sysclock.date.day_of_week > 7) sysclock.date.day_of_week = 1;
      }
    }
  }

  if(session_timer) session_timer--;

  if(system_output_timer)
  {
    system_output_timer--;
    if(!system_output_timer) system_output_timer_exp_flag = 1;
  }

  if(system_sync_clock_timer)
  {
    system_sync_clock_timer--;
    if(!system_sync_clock_timer) system_sync_clock_flag = 1;
  }

  if(device_config.output_config == OUT_EMERGENCY) led1_tog();
  else led3_tog();
}

void device_super_cycle()
{
//  syslog_obj();
//  syslog("f = %d, t = %d", system_sync_clock_flag, system_sync_clock_timer);

  if(system_sync_clock_flag)
  {
    device_load_clock();

    system_sync_clock_flag = 0;
    system_check_rules_flag = 1;
    system_sync_clock_timer = DEVICE_SYNC_CLOCK_TIMER_MIN;

    syslog("system_sync_clock_flag = 0");
  }
  if(system_check_rules_flag)
  {
    system_check_rules_flag = 0;
    device_check_rules();
    syslog("system_check_rules_flag = 0");
  }
  if(system_output_timer_exp_flag)
  {
    system_output_timer_exp_flag = 0;
    if(device_config.output_config != OUT_MANUAL) sys_set_output(0);
    syslog("system_output_timer_exp_flag = 0");
  }
}

void device_update_baudrate()
{
  syslog_obj();

  uart2_deinit();
  uart2_init(if_baudrates[device_config.baudrate]);
}

uint8_t device_save_clock()
{
  syslog_obj();

  last_output_status = 0;
  system_output_timer_exp_flag = 0;
  system_output_timer = 0;
  system_sync_clock_timer = DEVICE_SYNC_CLOCK_TIMER_MIN;

  // update dow
  sysclock.date.day_of_week = rtc_calc_day_of_week(&sysclock);

  uint8_t st = rtc_set(&sysclock);
  if(st)
  {
    device_update_output_config(OUT_EMERGENCY);
    syslog("i2c error");
  }
  else
  {
      syslog("saved time: %d:%d:%d, date: %d/%d/%d", sysclock.time.hour, sysclock.time.min, sysclock.time.sec,
        sysclock.date.day, sysclock.date.month, sysclock.date.year + 2000);
  }
  return st;
}

uint8_t device_load_clock()
{
  syslog_obj();

  uint8_t st = rtc_init();
  if(st)
  {
    syslog("i2c error when try to init clock");
    device_update_output_config(OUT_EMERGENCY);
  }

  st = rtc_get(&sysclock);
  if(st)
  {
    device_update_output_config(OUT_EMERGENCY);
    syslog("i2c error");
  }
  else
  {
    if(adc1_get_bat_v() <= M41T81_MIN_BATTAY_VOLTAGE)
    {
      device_update_output_config(OUT_EMERGENCY);
      syslog("bad battary voltage");
    }
    if(rtc_validate(&sysclock, 1))
    {
      device_update_output_config(OUT_EMERGENCY);
      syslog("validate rtc error");
    }
    syslog("loaded time: %d:%d:%d, date: %d/%d/%d", sysclock.time.hour, sysclock.time.min, sysclock.time.sec,
      sysclock.date.day, sysclock.date.month, sysclock.date.year + 2000);
  }
  return st;
}

void device_save_cfg()
{
  syslog_obj();

  uint32_t crc;

  eeprom_write(0x20, (uint8_t*)&device_config, sizeof(device_config));
  eeprom_write(0x40, (uint8_t*)&rules, sizeof(rules));

  CRC_ResetDR();
  CRC_CalcBlockCRC((uint32_t*)&device_config, sizeof(device_config) >> 2);
  CRC_CalcBlockCRC((uint32_t*)&rules, sizeof(rules) >> 2);
  CRC_CalcBlockCRC((uint32_t*)&sw_version, sizeof(sw_version) >> 2);
  crc = CRC_GetCRC();

  syslog("crc: 0x%08x", crc);

  eeprom_write(0, (uint8_t*)&crc, sizeof(crc));
}

void device_load_cfg()
{
  syslog_obj();

  uint32_t crc, crc_stored;

  eeprom_read(0, (uint8_t*)&crc_stored, sizeof(crc_stored));
  eeprom_read(0x20, (uint8_t*)&device_config, sizeof(device_config));
  eeprom_read(0x40, (uint8_t*)rules, sizeof(rules));

  CRC_ResetDR();
  CRC_CalcBlockCRC((uint32_t*)&device_config, sizeof(device_config) >> 2);
  CRC_CalcBlockCRC((uint32_t*)&rules, sizeof(rules) >> 2);
  CRC_CalcBlockCRC((uint32_t*)&sw_version, sizeof(sw_version) >> 2);
  crc = CRC_GetCRC();

  syslog("crc: 0x%08x, crc_stored: 0x%08x", crc, crc_stored);

  // restore default settings, if crc error
  if(crc != crc_stored) device_default_cfg();

  device_update_baudrate();
}

#define RULE_TIME(obj) (((obj).day_of_week - 1) * 24 * 60 + (obj).hour * 60 + (obj).min)
#define DEVICE_TIME(obj) (((obj).date.day_of_week - 1) * 24 * 60 + (obj).time.hour * 60 + (obj).time.min)

int rules_compare(const void *a, const void *b)
{
  int av = RULE_TIME(*((device_rule_t *)a));
  int bv = RULE_TIME(*((device_rule_t *)b));

  if(!(((device_rule_t *)a)->day_of_week)) av = 24 * 60 * 8;
  if(!(((device_rule_t *)b)->day_of_week)) bv = 24 * 60 * 8;

  if(av == bv) return 0;
  else if(av < bv) return -1;
  else return 1;
}

static void rules_sort()
{
  syslog_obj();

  // clear unused data
  int i;
  for(i = 1; i < SIZE_OF_ARRAY(rules); i++)
  {
    if(!rules[i].day_of_week)
    {
      rules[i].hour = 0;
      rules[i].min = 0;
      rules[i].status_mask = 0;
    }
  }

  qsort(rules, DEVICE_RULES_NUM, sizeof(device_rule_t), rules_compare);
}

// return (index + 1 and 0 when err)
uint8_t device_get_active_rule_number()
{
  syslog_obj();

  #define CHECK_RULE(a, b, c) (RULE_TIME(a) <= DEVICE_TIME(b) && DEVICE_TIME(b) < RULE_TIME(c))

  // no data ([0] not exist)
  if(!rules[0].day_of_week)
  {
//    syslog("rules: no data");
    return 0;
  }
  // if only one rule
  if(!rules[1].day_of_week)
  {
//    syslog("rules: only one rule");
    return 1;
  }

  int i;
  for(i = 1; i < SIZE_OF_ARRAY(rules); i++)
  {
    if(!rules[i].day_of_week)
    {
//      syslog("rules: found end of rules in index: %d", i);
      break;
    }
    if(CHECK_RULE(rules[i - 1], sysclock, rules[i]))
    {
//      syslog("rules: linear found: %d", i);
      return i;
    }
  }

  // check tail and head
  if(!CHECK_RULE(rules[0], sysclock, rules[i - 1]))
  {
//    syslog("rules: tail and head found: %d", i);
    return i;
  }

  return 0;
}

void device_set_output_timeout(uint8_t output_status)
{
  syslog_obj();

  if(output_status != last_output_status)
  {
    last_output_status = output_status;
    sys_set_output(output_status);

    system_output_timer = device_config.ctrl_wait_sec;
    system_output_timer_exp_flag = 0;
  }
  else syslog("new state ignored");
}

void device_update_output_config(output_config_e state)
{
  syslog_obj();
  syslog("new state: %s", output_config_e_str[state]);

  device_config.output_config = state;
  device_save_cfg();
  led1_dis();
  led3_dis();

  if(state == OUT_MANUAL) sys_set_output(0);
  else if(state == OUT_AUTOMATIC || state == OUT_EMERGENCY) device_check_rules();
}

void device_check_rules()
{
  syslog_obj();

  if(device_config.output_config == OUT_EMERGENCY) device_set_output_timeout(CTRL1(CW) | CTRL2(CW));
  else if(device_config.output_config == OUT_AUTOMATIC)
  {
    uint8_t index = device_get_active_rule_number();
    if(!index) return;
    index -= 1;

    syslog("apply index: %d", index);
    device_set_output_timeout(rules[index].status_mask);
  }
}

void device_pkg_handler(if_cmd_e cmd, if_pkg_t *pkg, if_access_type_e access_type)
{
//  syslog_obj();

  if((cmd & 0x7f) != CMD_OPEN)
  {
    if(!session_timer)
    {
//      syslog("session not opened (or session timer up)!");
      return;
    }
    else session_timer = MIN_TO_SEC(device_config.session_timer);
  }

  switch(cmd & 0x7f)
  {
    case CMD_OPEN:
      if(!ncmp(pkg->data, device_config.pwd, DEVICE_PWD_LEN))
      {
        session_timer = MIN_TO_SEC(device_config.session_timer);
//        syslog("session opened!");
        device_send_status(cmd, ST_OK);
      }
      else
      {
//        syslog("session not opened, bad pwd!");
        session_timer = 0;
        device_send_status(cmd, ST_BAD_PWD);
      }
      break;

    case CMD_RESET_CONFIG:
//      syslog("loaded defaul config!");
      device_default_cfg();
      device_check_rules();
      device_send_status(cmd, ST_OK);
      break;

    case CMD_CLOSE:
//      syslog("session closed!");
      device_send_status(cmd, ST_OK);
      session_timer = 0;
      break;

    case CMD_SOFTWARE_VERSION:
    {
//      syslog("version request!");
      if_pkg_t ans = { (uint8_t*)&sw_version, sizeof(version_t) };
      device_send_data(cmd, &ans);
    }
    break;

    case CMD_OUTPUT_CONFIG:
      if(access_type == OBJ_WR)
      {
        if(pkg->data[0] >= OUT_MAX)
        {
          device_send_status(cmd, ST_BAD_DATA);
          break;
        }
        last_output_status = 0;
        device_update_output_config(*pkg->data);
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.output_config, sizeof(device_config.output_config) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_OUTPUT_CTRL:
      if(access_type == OBJ_WR)
      {
        if(device_config.output_config == OUT_MANUAL)
        {
          sys_set_output(*pkg->data);
          device_send_status(cmd, ST_OK);
        }
        else
        {
          device_send_status(cmd, ST_BAD_LOCK_STATUS);
        }
      }
      else if(access_type == OBJ_RD)
      {
        uint8_t status;
        if(device_config.output_config == OUT_MANUAL) status = sys_get_output();
        else status = last_output_status;
        if_pkg_t ans = { (uint8_t*)&status, sizeof(status) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_BAUDRATE:
      if(access_type == OBJ_WR)
      {
        if(pkg->data[0] >= BAUD_MAX)
        {
          device_send_status(cmd, ST_BAD_DATA);
          break;
        }
        memcpy(&device_config.baudrate, pkg->data, sizeof(device_config.baudrate));
        device_update_baudrate();
        device_save_cfg();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.baudrate, sizeof(device_config.baudrate) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_CLOCK:
      if(access_type == OBJ_WR)
      {
//        syslog("write clock cmd");
        if(rtc_validate((device_rtc_t *)pkg->data, 0)) device_send_status(cmd, ST_BAD_DATA);
        else
        {
          memcpy(&sysclock, pkg->data, sizeof(device_rtc_t));
          device_save_clock();
          device_check_rules();
          device_send_status(cmd, ST_OK);
        }
      }
      else if(access_type == OBJ_RD)
      {
//        syslog("read clock cmd");
        if_pkg_t ans = { (uint8_t*)&sysclock, sizeof(device_rtc_t) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_SERIAL_NUMBER:
      if(access_type == OBJ_WR)
      {
        memcpy(&device_config.serial_number, pkg->data, sizeof(device_config.serial_number));
        device_save_cfg();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.serial_number, sizeof(device_config.serial_number) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_PWD:
      if(access_type == OBJ_WR)
      {
        memcpy(device_config.pwd, pkg->data, sizeof(device_config.pwd));
        device_save_cfg();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.pwd, sizeof(device_config.pwd) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_RULES:
      if(access_type == OBJ_WR)
      {
        memcpy(rules, pkg->data, sizeof(rules));
        rules_sort();
        device_save_cfg();
        last_output_status = 0;
        device_check_rules();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)rules, sizeof(rules) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_MCU_TEMPERATURE:
    {
        int16_t val = adc1_get_temp() * 100;
        if_pkg_t ans = { (uint8_t*)&val, sizeof(val) };
        device_send_data(cmd, &ans);
    }
    break;

    case CMD_BATTARY_VOLTAGE:
    {
        uint16_t val = adc1_get_bat_v() * 100;
        if_pkg_t ans = { (uint8_t*)&val, sizeof(val) };
        device_send_data(cmd, &ans);
    }
    break;

    case CMD_ACTIVE_RULE:
    {
        uint8_t val = device_get_active_rule_number();
        if_pkg_t ans = { (uint8_t*)&val, sizeof(val) };
        device_send_data(cmd, &ans);
    }
    break;

    case CMD_DUBUG_MODE:
      if(access_type == OBJ_WR)
      {
        if(pkg->data[0] >= DEBUG_MAX)
        {
          device_send_status(cmd, ST_BAD_DATA);
          break;
        }
        memcpy(&device_config.debug_mode, pkg->data, sizeof(device_config.debug_mode));
        device_save_cfg();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.debug_mode, sizeof(device_config.debug_mode) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_CTRL_WAIT_SEC:
      if(access_type == OBJ_WR)
      {
        if(pkg->data[0] < DEVICE_CTRL_WAIT_SEC_MIN || pkg->data[0] > DEVICE_CTRL_WAIT_SEC_MAX)
        {
          device_send_status(cmd, ST_BAD_DATA);
          break;
        }
        memcpy(&device_config.ctrl_wait_sec, pkg->data, sizeof(device_config.ctrl_wait_sec));
        device_save_cfg();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.ctrl_wait_sec, sizeof(device_config.ctrl_wait_sec) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_SESSION_TIME_MIN:
      if(access_type == OBJ_WR)
      {
        if(pkg->data[0] < DEVICE_SESSION_MIN_MIN || pkg->data[0] > DEVICE_SESSION_MIN_MAX)
        {
          device_send_status(cmd, ST_BAD_DATA);
          break;
        }
        memcpy(&device_config.session_timer, pkg->data, sizeof(device_config.session_timer));
        device_save_cfg();
        device_send_status(cmd, ST_OK);
      }
      else if(access_type == OBJ_RD)
      {
        if_pkg_t ans = { (uint8_t*)&device_config.session_timer, sizeof(device_config.session_timer) };
        device_send_data(cmd, &ans);
      }
      break;

    case CMD_SELFTEST:
      device_send_status(cmd, ST_OK);
    break;

    case CMD_SERIAL_NUMBER_TEXT:
    {
      char sn_str[11];
      int addr;
      memcpy(&addr, device_config.serial_number, sizeof(device_config.serial_number));
      snprintf(sn_str, sizeof(sn_str), "%010u", addr);
      if_pkg_t ans = { (uint8_t*)sn_str, sizeof(sn_str) };
      device_send_data(cmd, &ans);
    }
    break;

    default:
      break;
  }
}
