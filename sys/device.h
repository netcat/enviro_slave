#ifndef DEVICE_H
#define DEVICE_H

#include "sys.h"
#include "efs.h"

// to device
/*
  sn cd ar cc

  x - device serial number 16 bytes
  cd - cmd (0x80 if write)
  ar - arg
  cc - crc16
*/

// from device
/*
  sn st cd sz dt cc
  
  x - device serial number 16 bytes
  st - status
  cd - cmd
  sz - size of data
  dt - data
  cc - crc16
*/

#define DEVICE_PWD_LEN 8
#define DEVICE_SERIAL_NUMBER_LEN 4
#define DEVICE_RULES_NUM 16

#define DEVICE_ACCESS_COMMAND_LEN 1
#define DEVICE_DATA_SIZE_LEN 1
#define DEVICE_CMD_STATUS_LEN 1
#define DEVICE_CRC_LEN sizeof(uint16_t)
#define DEVICE_PKG_SERVICE_SIZE (DEVICE_SERIAL_NUMBER_LEN + DEVICE_ACCESS_COMMAND_LEN + DEVICE_CRC_LEN)
#define DEVICE_PKG_ANS_SIZE (DEVICE_PKG_SERVICE_SIZE + DEVICE_CMD_STATUS_LEN + DEVICE_DATA_SIZE_LEN)

#define BROADCAST_ADDR 0xff, 0xff, 0xff, 0xff
#define DEFAULT_ADDR 0x01, 0x00, 0x00, 0x00
#define DEFAULT_PWD 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

#define DEVICE_SESSION_MIN_DEFAULT 10
#define DEVICE_SESSION_MIN_MIN 1
#define DEVICE_SESSION_MIN_MAX 60

#define DEVICE_CTRL_WAIT_SEC_DEFAULT 10
#define DEVICE_CTRL_WAIT_SEC_MIN 5
#define DEVICE_CTRL_WAIT_SEC_MAX 30

#define DEVICE_CTRL_WAIT_SEC_DEFAULT 10
#define DEVICE_CTRL_WAIT_SEC_MIN 5
#define DEVICE_CTRL_WAIT_SEC_MAX 30

#define DEVICE_SYNC_CLOCK_TIMER_MIN (30 * 60)

#define CW 0x01
#define CCW 0x02

#define CTRL1(val) ((val) << 2)
#define CTRL2(val) (val)

#define SIZE_OF_ARRAY(a) (sizeof(a) / sizeof(a[0]))
#define MIN_TO_SEC(t) ((t) * 60)

#ifdef DBG
#define syslog_obj() syslog("---> %s (line: %d)", __FUNCTION__, __LINE__)
#else
#define syslog(str, ...)
#define syslog_obj()
#endif

typedef struct
{
  uint8_t serial_number[DEVICE_SERIAL_NUMBER_LEN];
  uint8_t pwd[DEVICE_PWD_LEN];
  uint8_t baudrate;
  uint8_t output_config;
  uint8_t session_timer;
  uint8_t bad_pwd_lock;
  uint8_t debug_mode;
  uint8_t ctrl_wait_sec;
} device_config_t;

typedef struct
{
  uint8_t hour;
  uint8_t min;
  uint8_t day_of_week;
  uint8_t status_mask;
} device_rule_t;

typedef enum
{
  CMD_OPEN, // 0
  CMD_CLOSE, // 1
  CMD_SERIAL_NUMBER, // 2
  CMD_CLOCK, // 3
  CMD_BAUDRATE, // 4
  CMD_SOFTWARE_VERSION, // 5
  CMD_RESET_CONFIG, // 6
  CMD_PWD, // 7
  CMD_RULES, // 8
  CMD_OUTPUT_CTRL, // 9
  CMD_OUTPUT_CONFIG, // A
  CMD_MCU_TEMPERATURE, // B
  CMD_BATTARY_VOLTAGE, // C
  CMD_ACTIVE_RULE, // D

  CMD_DUBUG_MODE, // E
  CMD_CTRL_WAIT_SEC, // F
  CMD_SESSION_TIME_MIN, // 10
  CMD_SERIAL_NUMBER_TEXT, // 11
  CMD_SELFTEST, // 12

  CMD_MAX,

  CMD_LOG_MSG = 0x7F,
} if_cmd_e;

static const char *if_cmd_e_str[] __attribute__ ((unused)) =
{
  "CMD_OPEN",
  "CMD_CLOSE",
  "CMD_SERIAL_NUMBER",
  "CMD_CLOCK",
  "CMD_BAUDRATE",
  "CMD_SOFTWARE_VERSION",
  "CMD_RESET_CONFIG",
  "CMD_PWD",
  "CMD_RULES",
  "CMD_OUTPUT_CTRL",
  "CMD_OUTPUT_CONFIG",
  "CMD_MCU_TEMPERATURE",
  "CMD_BATTARY_VOLTAGE",
  "CMD_ACTIVE_RULE",

  "CMD_DUBUG_MODE",
  "CMD_CTRL_WAIT_SEC",
  "CMD_SESSION_TIME_MIN",
  "CMD_SERIAL_NUMBER_TEXT",
  "CMD_SELFTEST",
};

typedef enum
{
  ST_OK,
  ST_DBG,
  ST_BAD_SIZE,
  ST_BAD_DATA_SIZE,
  ST_BAD_CRC,
  ST_BAD_CMD,
  ST_BAD_PWD,
  ST_BAD_DATA,
  ST_BAD_ACCESS_TYPE,
  ST_BAD_LOCK_STATUS,

  ST_MAX
} if_status_e;

static const char *if_status_e_str[] __attribute__ ((unused)) =
{
  "ST_OK",
  "ST_DBG,",
  "ST_BAD_SIZE",
  "ST_BAD_DATA_SIZE",
  "ST_BAD_CRC",
  "ST_BAD_CMD",
  "ST_BAD_PWD",
  "ST_BAD_DATA",
  "ST_BAD_ACCESS_TYPE",
  "ST_BAD_LOCK_STATUS",
};

typedef enum
{
  OBJ_RD = 0x01,
  OBJ_WR = 0x02,
  OBJ_RW = (OBJ_RD | OBJ_WR),
} if_access_type_e;

static const char *access_type_str[] __attribute__ ((unused)) =
{ "", "r", "w", "rw" };

typedef struct
{
  if_cmd_e command;
  if_access_type_e access_type;
  int32_t data_size;
} interface_obj_t;

typedef struct
{
  uint8_t *data;
  uint32_t size;
} if_pkg_t;

typedef struct __attribute__ ((__packed__))
{
  uint8_t hour;
  uint8_t min;
  uint8_t sec;
} device_time_t;

typedef struct __attribute__ ((__packed__))
{
  uint8_t day_of_week;
  uint8_t day;
  uint8_t month;
  uint8_t year;
} device_date_t;

typedef struct __attribute__ ((__packed__))
{
  device_time_t time;
  device_date_t date;
} device_rtc_t;

typedef enum
{
  BAUD_1200,
  BAUD_2400,
  BAUD_4800,
  BAUD_9600,
  BAUD_14400,
  BAUD_19200,
  BAUD_38400,
  BAUD_57600,

  BAUD_MAX,
} device_baudrate_e;

static const uint32_t if_baudrates[BAUD_MAX] __attribute__ ((unused)) =
{
  1200,
  2400,
  4800,
  9600,
  14400,
  19200,
  38400,
  57600,
};

typedef enum
{
  DEBUG_DISABLE,
  DEBUG_ENABLE,

  DEBUG_MAX,
} debug_mode_e;

typedef enum
{
  OUT_MANUAL,
  OUT_AUTOMATIC,
  OUT_EMERGENCY,

  OUT_MAX,
} output_config_e;

static const char *output_config_e_str[] __attribute__ ((unused)) =
{
  "OUT_MANUAL",
  "OUT_AUTOMATIC",
  "OUT_EMERGENCY",
};
#ifdef DBG
void syslog(const char *fmt, ...);
#endif
void device_receive(if_pkg_t *pkg);
void device_send_data(if_cmd_e cmd, if_pkg_t *pkg);
void device_send_status(if_cmd_e cmd, if_status_e status);
void device_save_cfg();
void device_update_baudrate();
uint8_t device_save_clock();
if_status_e device_check_pwd(if_pkg_t *pkg);
void device_pkg_handler(if_cmd_e cmd, if_pkg_t *pkg, if_access_type_e access_type);
void device_default_cfg();
void device_check_rules();
uint8_t device_load_clock();
void device_load_cfg();
void device_super_cycle();
void device_systimer_tick();
void device_update_output_config(output_config_e state);
void device_set_output_timeout(uint8_t output_status);

#endif
