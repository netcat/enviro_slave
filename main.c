#include "sys.h"
#include "device.h"
#include "rtc.h"

uint8_t rx2_buf_cnt = 0, rx2_buf[IF_BUF_SIZE];
uint8_t event_full_buf2 = 0;

void uart2_receive(uint8_t data)
{
	if(!event_full_buf2)
	{
		if(!rx2_buf_cnt) tmr2_start();
		rx2_buf[rx2_buf_cnt++] = data;
		if(rx2_buf_cnt > IF_BUF_SIZE) rx2_buf_cnt = 0;
		tmr2_reset();
	}
}

void tmr2_tick()
{
	if(rx2_buf_cnt)
	{
	  tmr2_dis();
		event_full_buf2 = 1;
	}
}

void tmr7_tick()
{
  device_systimer_tick();
}

void clr_buf2()
{
  tmr2_reset();
  rx2_buf_cnt = 0;
	event_full_buf2 = 0;
}

void init()
{
  brd_init();
  rtc_init();

#ifdef DBG
  // for first debug messages
  uart2_init(if_baudrates[BAUD_9600]);
#endif

  syslog("init: system start");
  device_load_cfg();
  tmr7_start();
}

int main()
{
  init();

	while(1)
	{
	  device_super_cycle();

    if(event_full_buf2)
    {
      led0_blink();
      // fixme ???
      if(rx2_buf_cnt > 2)
      {
        if_pkg_t pkg = { rx2_buf, rx2_buf_cnt };
        device_receive(&pkg);
      }
      clr_buf2();
    }
	}

	return 0;
}
