#ifndef SYS_H
#define SYS_H

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_crc.h"
#include "misc.h"
#include "crc16.h"

#define IF_BUF_SIZE 256

#define ADC_V25 1.41
#define ADC_VREF 3.3
#define ADC_AVG_SCOPE 0.0043

#define DEVICE_TIMOUT 50

#define LED_GPIO_PORT GPIOC
#define LED_GPIO_PORT2 GPIOB
#define LED0 GPIO_Pin_12
#define LED1 GPIO_Pin_11
#define LED2 GPIO_Pin_10
#define LED3 GPIO_Pin_3

#define CTRL0_GPIO_PORT GPIOA
#define CTRL1_GPIO_PORT GPIOC

#define CTRL00 GPIO_Pin_6
#define CTRL01 GPIO_Pin_7
#define CTRL10 GPIO_Pin_4
#define CTRL11 GPIO_Pin_5

#define i2c2_timeout(x) timeout_cnt = 0xFFFF; while (x) { if (timeout_cnt-- == 0) goto errReturn; }

void delay_cyc(volatile uint32_t cnt);

extern void uart2_receive(uint8_t data);
void uart2_init(uint32_t baudrate);
void uart2_deinit();
void uart2_out(uint8_t data);
void uart2_tx(uint8_t *data, uint8_t cnt);
void uart2_tx_mode();
void uart2_rx_mode();

void brd_init();
void led0_en();
void led1_en();
void led2_en();
void led3_en();
void led0_dis();
void led1_dis();
void led2_dis();
void led3_dis();
void led0_tog();
void led1_tog();
void led2_tog();
void led3_tog();
void led0_blink();
void led2_blink();
void led3_blink();

extern void tmr7_tick();
void tmr7_init();
void tmr7_reset();
void tmr7_dis();
void tmr7_start();

extern void tmr2_tick();
void tmr2_init();
void tmr2_reset();
void tmr2_dis();
void tmr2_start();

void sys_crc_init();
uint32_t sys_crc_calc(uint32_t *buffer, uint32_t buff_len);

void i2c2_init();
uint8_t i2c2_write(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, const uint8_t *buf, uint8_t len);
uint8_t i2c2_read(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len);

void wait_ms(uint32_t ms);

void adc1_init();
uint16_t adc1_get(uint8_t ch);
float adc1_get_temp();
float adc1_get_bat_v();

void sys_set_output(uint8_t st);
uint8_t sys_get_output();

int ncmp(const uint8_t *d1, const uint8_t *d2, int size);

#endif
