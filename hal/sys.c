#include <stdio.h>
#include <string.h>

#include "sys.h"
#include "device.h"

void delay_cyc(volatile uint32_t cnt) { while(cnt--); }

void wait_ms(volatile uint32_t ms)
{
  while(ms--) delay_cyc(3000);
}

void USART2_IRQHandler(void)
{
	uint8_t data;
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
		data = USART_ReceiveData(USART2);
		uart2_receive(data);
	}
}

void uart2_init(uint32_t baudrate)
{
	USART_InitTypeDef USART_InitStructure;

	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	GPIO_InitTypeDef GPIO_InitStructure;

	// Configure USART2_TX as 'alternate function push-pull'
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Configure USART1_RX as 'input floating'
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Configure DE
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Configuring and enabling USART2
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	USART_Init(USART2, &USART_InitStructure);
	USART_Cmd(USART2, ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART2, USART_IT_TC, DISABLE);
}

void uart2_deinit()
{
  USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
  USART_DeInit(USART2);
  USART_Cmd(USART2, DISABLE);
}

void uart2_out(uint8_t data)
{
  while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
	USART_SendData(USART2, data);
}

void uart2_tx(uint8_t *data, uint8_t cnt)
{
	uart2_tx_mode();

	while(cnt--)
	{
	  while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
		USART_SendData(USART2, *data++);
	}
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);

	uart2_rx_mode();
	led2_blink();
}

void uart2_tx_mode()
{
//  USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
	GPIO_SetBits(GPIOA, GPIO_Pin_1);
}

void uart2_rx_mode()
{
  USART_ClearFlag(USART2, USART_FLAG_RXNE);
	GPIO_ResetBits(GPIOA, GPIO_Pin_1);

//	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}

void brd_init()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = LED0 | LED1 | LED2;
	GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = LED3;
	GPIO_Init(LED_GPIO_PORT2, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin = CTRL00 | CTRL01;
  GPIO_Init(CTRL0_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin = CTRL10 | CTRL11;
  GPIO_Init(CTRL1_GPIO_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  i2c2_init();

	// mdls
//  uart2_init(9600);
	tmr2_init();
	tmr7_init();

	adc1_init();

	sys_crc_init();

	led0_dis();
	led1_dis();
	led2_dis();
	led3_dis();
}

void led0_en()
{
	GPIO_SetBits(LED_GPIO_PORT, LED0);
}

void led1_en()
{
	GPIO_SetBits(LED_GPIO_PORT, LED1);
}

void led2_en()
{
	GPIO_SetBits(LED_GPIO_PORT, LED2);
}

void led3_en()
{
	GPIO_SetBits(LED_GPIO_PORT2, LED3);
}

void led0_dis()
{
	GPIO_ResetBits(LED_GPIO_PORT, LED0);
}

void led1_dis()
{
	GPIO_ResetBits(LED_GPIO_PORT, LED1);
}

void led2_dis()
{
	GPIO_ResetBits(LED_GPIO_PORT, LED2);
}

void led3_dis()
{
	GPIO_ResetBits(LED_GPIO_PORT2, LED3);
}

void led0_tog()
{
	LED_GPIO_PORT->ODR ^= LED0;
}

void led0_blink()
{
	LED_GPIO_PORT->ODR ^= LED0;
	delay_cyc(5000);
	LED_GPIO_PORT->ODR ^= LED0;
}

void led3_blink()
{
	LED_GPIO_PORT2->ODR ^= LED3;
	delay_cyc(5000);
	LED_GPIO_PORT2->ODR ^= LED3;
}

void led1_tog()
{
	LED_GPIO_PORT->ODR ^= LED1;
}

void led2_tog()
{
	LED_GPIO_PORT->ODR ^= LED2;
}

void led3_tog()
{
	LED_GPIO_PORT2->ODR ^= LED3;
}

void led2_blink()
{
	LED_GPIO_PORT->ODR ^= LED2;
	delay_cyc(5000);
	LED_GPIO_PORT->ODR ^= LED2;
}

// TMR7

void tmr7_init()
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
	// 8mhz / 8000 = 1khz
	TIM7->PSC = 8000 - 1; // prescaler
	TIM7->ARR = 1000; // tmr limit
	TIM7->DIER |= TIM_DIER_UIE; // allow interrupt
	tmr7_reset();
	tmr7_dis();

	TIM7->EGR |= TIM_EGR_UG;
	TIM7->SR = (uint16_t)~TIM_IT_Update;

	NVIC_EnableIRQ(TIM7_IRQn); // allow TIM1_IRQn
}

void tmr7_start()
{
	TIM7->CNT = 0; // reset counter
	TIM7->CR1 |= TIM_CR1_CEN; // start
}

void tmr7_reset()
{
	TIM7->CNT = 0; // reset counter
}

void tmr7_dis()
{
	TIM7->CR1 &= ~TIM_CR1_CEN; // stop
}

void TIM7_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM7, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
		tmr7_tick();
	}
}

// TMR2

void tmr2_init()
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	// 8mhz / 8000 = 1khz
	TIM2->PSC = 8000 - 1; // prescaler
	TIM2->ARR = DEVICE_TIMOUT; // ms
	TIM2->DIER |= TIM_DIER_UIE; // allow interrupt
	tmr2_reset();
	tmr2_dis();

	TIM2->EGR |= TIM_EGR_UG;
	TIM2->SR = (uint16_t)~TIM_IT_Update;

	NVIC_EnableIRQ(TIM2_IRQn); // allow TIM2_IRQn
}

void tmr2_start()
{
	TIM2->CNT = 0; // reset counter
	TIM2->CR1 |= TIM_CR1_CEN; // start
}

void tmr2_reset()
{
	TIM2->CNT = 0; // reset counter
}

void tmr2_dis()
{
	TIM2->CR1 &= ~TIM_CR1_CEN; // stop
}

void TIM2_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		tmr2_tick();
	}
}

void i2c2_init()
{
  GPIO_InitTypeDef GPIO_InitStructure;
  I2C_InitTypeDef I2C_InitStructure;

  /* I2C2 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);

  // Configure I2C2 GPIOs
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10 | GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  /* Configure I2C2 */
  /* I2C DeInit */
  I2C_DeInit(I2C2);

  /* Set the I2C structure parameters */
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
  I2C_InitStructure.I2C_OwnAddress1 = 0;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  I2C_InitStructure.I2C_ClockSpeed = 5000;

  /* Initialize the I2C peripheral w/ selected parameters */
  I2C_Init(I2C2, &I2C_InitStructure);
  /* Enable the I2C peripheral */
  I2C_Cmd(I2C2, ENABLE);
}

uint8_t i2c2_write(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, const uint8_t *buf, uint8_t len)
{
  uint32_t timeout_cnt = 0;

  i2c2_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_BUSY));

  // start
  I2C_GenerateSTART(I2C2, ENABLE);
  i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));

  // dev addr
  I2C_Send7bitAddress(I2C2, addr, I2C_Direction_Transmitter);
  i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2C_SendData(I2C2, reg >> 24);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg >> 16);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg >> 8);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else if(reg_addr_size == 2)
  {
    I2C_SendData(I2C2, reg >> 8);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else
  {
    I2C_SendData(I2C2, reg);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }

  // send data
  while(len--)
  {
    I2C_SendData(I2C2, *buf++);
    i2c2_timeout(!I2C_GetFlagStatus(I2C2, I2C_FLAG_BTF));
  }

  I2C_GenerateSTOP(I2C2, ENABLE);
  i2c2_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_STOPF));
  return 0;

  errReturn:
  I2C_GenerateSTOP(I2C2, ENABLE);
  i2c2_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_STOPF));
  return 1;
}

uint8_t i2c2_read(uint8_t addr, uint32_t reg, uint8_t reg_addr_size, uint8_t *buf, uint32_t len)
{
  uint32_t timeout_cnt = 0;
  if(!len) return 0;
  i2c2_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_BUSY));

  // start
  I2C_GenerateSTART(I2C2, ENABLE);
  i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));

  // addr
  I2C_Send7bitAddress(I2C2, addr, I2C_Direction_Transmitter);
  i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  // mem addr
  if(reg_addr_size == 4)
  {
    I2C_SendData(I2C2, reg >> 24);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg >> 16);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg >> 8);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else if(reg_addr_size == 2)
  {
    I2C_SendData(I2C2, reg >> 8);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
    I2C_SendData(I2C2, reg);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  else
  {
    I2C_SendData(I2C2, reg);
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }

  // restart
  I2C_GenerateSTART(I2C2, ENABLE);
  i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));

  // Send Address
  I2C_Send7bitAddress(I2C2, addr, I2C_Direction_Receiver);
  i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));

  // recv data
  I2C_AcknowledgeConfig(I2C2, ENABLE);
  while(len--)
  {
    i2c2_timeout(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED));
    *buf++ = I2C_ReceiveData(I2C2);
  }
  I2C_AcknowledgeConfig(I2C2, DISABLE);

  // Wait for stop
  I2C_GenerateSTOP(I2C2, ENABLE);
  i2c2_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_STOPF));
  return 0;

  errReturn:
  I2C_GenerateSTOP(I2C2, ENABLE);
  i2c2_timeout(I2C_GetFlagStatus(I2C2, I2C_FLAG_STOPF));
  return 1;
}

void adc1_init()
{
  ADC_InitTypeDef ADC_InitStructure;
  ADC_StructInit(&ADC_InitStructure);

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

  ADC_DeInit(ADC1);

  ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfChannel = 1;
  ADC_Init(ADC1, &ADC_InitStructure);
  ADC_Cmd(ADC1, ENABLE);

  ADC_ResetCalibration(ADC1);
  while(ADC_GetResetCalibrationStatus(ADC1));
  ADC_StartCalibration(ADC1);
  while(ADC_GetCalibrationStatus(ADC1));
}

uint16_t adc1_get(uint8_t ch)
{
  ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_55Cycles5);
  ADC_SoftwareStartConvCmd(ADC1, ENABLE);
  while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
  return ADC_GetConversionValue(ADC1);
}

float adc1_get_temp()
{
  ADC_TempSensorVrefintCmd(ENABLE);

  uint16_t val = adc1_get(ADC_Channel_TempSensor);
  float U = (((float)val) / (1 << 12)) * ADC_VREF;
  float T = (ADC_V25 - U) / ADC_AVG_SCOPE + 25.0;

  ADC_TempSensorVrefintCmd(DISABLE);

  return T;
}

float adc1_get_bat_v()
{
  uint16_t val = adc1_get(ADC_Channel_10);
  float U = (((float)val) / (1 << 12)) * ADC_VREF;

  return U;
}

void sys_set_output(uint8_t st)
{
  syslog_obj();

  // ignore bad combinations
  if((st & 0x01) && (st & 0x02)) st &= 0x0C;
  if((st & 0x04) && (st & 0x08)) st &= 0x03;

  syslog("new state: 0x%02x", st);

  if(st & 0x01) GPIO_SetBits(CTRL0_GPIO_PORT, CTRL00);
  else GPIO_ResetBits(CTRL0_GPIO_PORT, CTRL00);
  if(st & 0x02) GPIO_SetBits(CTRL0_GPIO_PORT, CTRL01);
  else GPIO_ResetBits(CTRL0_GPIO_PORT, CTRL01);
  if(st & 0x04) GPIO_SetBits(CTRL1_GPIO_PORT, CTRL10);
  else GPIO_ResetBits(CTRL1_GPIO_PORT, CTRL10);
  if(st & 0x08) GPIO_SetBits(CTRL1_GPIO_PORT, CTRL11);
  else GPIO_ResetBits(CTRL1_GPIO_PORT, CTRL11);
}

uint8_t sys_get_output()
{
  uint8_t st = 0;
  st |= GPIO_ReadOutputDataBit(CTRL0_GPIO_PORT, CTRL00) ? 1 : 0;
  st |= GPIO_ReadOutputDataBit(CTRL0_GPIO_PORT, CTRL01) ? 2 : 0;
  st |= GPIO_ReadOutputDataBit(CTRL1_GPIO_PORT, CTRL10) ? 4 : 0;
  st |= GPIO_ReadOutputDataBit(CTRL1_GPIO_PORT, CTRL11) ? 8 : 0;

  return st;
}

int ncmp(const uint8_t *d1, const uint8_t *d2, int size)
{
  while(size--)
  {
    if(*d1++ != *d2++) return 1;
  }
  return 0;
}

void sys_crc_init()
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
}

uint32_t sys_crc_calc(uint32_t *buf, uint32_t len)
{
  CRC_ResetDR();
  return CRC_CalcBlockCRC(buf, len);
}
