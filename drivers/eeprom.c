#include "eeprom.h"

uint8_t eeprom_write(uint16_t addr, uint8_t *data, uint16_t count)
{
  uint8_t st = 0;
  while(count)
  {
    if(count >= EEPROM_24LC64_PAGE_SIZE)
    {
      st |= i2c2_write(EEPROM_24LC64_ADDR, addr, EEPROM_24LC64_REG_ADDR_SIZE, data, EEPROM_24LC64_PAGE_SIZE);
      wait_ms(EEPROM_24LC64_PAGE_WRITE_TIME_MS);
      count -= EEPROM_24LC64_PAGE_SIZE;
      data += EEPROM_24LC64_PAGE_SIZE;
      addr += EEPROM_24LC64_PAGE_SIZE;
    }
    else
    {
      st |= i2c2_write(EEPROM_24LC64_ADDR, addr, EEPROM_24LC64_REG_ADDR_SIZE, data, count);
      wait_ms(EEPROM_24LC64_PAGE_WRITE_TIME_MS);
      break;
    }
  }
  return st;
}

uint8_t eeprom_read(uint16_t addr, uint8_t *data, uint16_t count)
{
  uint8_t st = 0;
  while(count)
  {
    if(count >= EEPROM_24LC64_PAGE_SIZE)
    {
      st |= i2c2_read(EEPROM_24LC64_ADDR, addr, EEPROM_24LC64_REG_ADDR_SIZE, data, EEPROM_24LC64_PAGE_SIZE);
      wait_ms(EEPROM_24LC64_PAGE_WRITE_TIME_MS);
      count -= EEPROM_24LC64_PAGE_SIZE;
      data += EEPROM_24LC64_PAGE_SIZE;
      addr += EEPROM_24LC64_PAGE_SIZE;
    }
    else
    {
      st |= i2c2_read(EEPROM_24LC64_ADDR, addr, EEPROM_24LC64_REG_ADDR_SIZE, data, count);
      wait_ms(EEPROM_24LC64_PAGE_WRITE_TIME_MS);
      break;
    }
  }
  return st;
}

