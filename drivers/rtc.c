﻿#include "rtc.h"

uint8_t rtc_init()
{
  uint8_t st = 0;
  uint8_t config[] = {
    M41T81_REG_ALM_HOUR, 0,
    M41T81_REG_ALM_MON, 0,
    M41T81_REG_SQW, 0,
    M41T81_REG_ALM_HOUR, 0,
    M41T81_REG_CONTROL, 0
  };

  int i;
  for(i = 0; i < sizeof(config) >> 1; i++)
    st |= i2c2_write(M41T81_ADDR, config[i * 2], M41T81_REG_ADDR_SIZE, &config[i * 2 + 1], 1);

  return st;
}

uint8_t rtc_set(device_rtc_t *clock)
{
  uint8_t buf[7];
  uint8_t st = 0;

  buf[0] = BIN_TO_BCD(clock->time.sec);
  buf[1] = BIN_TO_BCD(clock->time.min);
  buf[2] = BIN_TO_BCD(clock->time.hour);
  buf[3] = BIN_TO_BCD(clock->date.day_of_week);
  buf[4] = BIN_TO_BCD(clock->date.day);
  buf[5] = BIN_TO_BCD(clock->date.month);
  buf[6] = BIN_TO_BCD(clock->date.year);

  st |= i2c2_write(M41T81_ADDR, M41T81_REG_TIMEKEEPER, M41T81_REG_ADDR_SIZE, (uint8_t*)buf, sizeof(buf));

  return st;
}

uint8_t rtc_get(device_rtc_t *clock)
{
  uint8_t buf[7];
  uint8_t st = i2c2_read(M41T81_ADDR, M41T81_REG_TIMEKEEPER, M41T81_REG_ADDR_SIZE, (uint8_t*)buf, sizeof(buf));
  if(!st)
  {
    clock->time.sec = BCD_TO_BIN(buf[0]);
    clock->time.min = BCD_TO_BIN(buf[1]);
    clock->time.hour = BCD_TO_BIN(buf[2]);
    clock->date.day_of_week = BCD_TO_BIN(buf[3]);
    clock->date.day = BCD_TO_BIN(buf[4]);
    clock->date.month = BCD_TO_BIN(buf[5]);
    clock->date.year = BCD_TO_BIN(buf[6]);
  }
  return st;
}

int rtc_calc_day_of_week(device_rtc_t *clock)
{
    int a = (14 - clock->date.month) / 12;
    int y = clock->date.year - a;
    int m = clock->date.month + 12 * a - 2;
    int dow = (7000 + (clock->date.day + y + (y >> 2) - y / 100 + y / 400 + (31 * m) / 12)) % 7;
    return dow ? dow : 7;
}

int rtc_validate(device_rtc_t *clock, int check_day_of_week)
{
  int months[] = { 30, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  // time
  if(clock->time.sec > 59 || clock->time.min > 59 || clock->time.hour > 23) return 1;
  // date
  if(check_day_of_week && (!clock->date.day_of_week || clock->date.day_of_week > 7)) return 1;
  if(!clock->date.month || clock->date.month > 12) return 1;
  if(!clock->date.day || clock->date.day > months[clock->date.month]) return 1;
  if(clock->date.year > 99) return 1;

  return 0;
}
