﻿#ifndef _DS1307_H_
#define _DS1307_H_

#include "sys.h"
#include "device.h"

#define M41T81_ADDR 0xD0

#define M41T81_REG_TIMEKEEPER 0x01
#define M41T81_REG_ALM_HOUR 0x0c
#define M41T81_REG_ALM_MON 0x0a
#define M41T81_REG_SQW 0x13
#define M41T81_REG_CONTROL 0x08

#define M41T81_REG_ADDR_SIZE 1

#define M41T81_MIN_BATTAY_VOLTAGE 2.5

#define BCD_TO_BIN(bcd) (((bcd) & 0x0f) + ((bcd) >> 4) * 10)
#define BIN_TO_BCD(dec) ((((dec) / 10) << 4) + ((dec) % 10))

uint8_t rtc_init();
uint8_t rtc_set(device_rtc_t *clock);
uint8_t rtc_get(device_rtc_t *clock);
int rtc_calc_day_of_week(device_rtc_t *clock);
int rtc_validate(device_rtc_t *clock, int check_day_of_week);

#endif

