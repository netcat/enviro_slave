#ifndef _EEPROM_H_
#define _EEPROM_H_

#include "sys.h"

#define EEPROM_24LC64_ADDR 0xA2
#define EEPROM_24LC64_PAGE_SIZE 32
#define EEPROM_24LC64_PAGE_WRITE_TIME_MS 5
#define EEPROM_24LC64_REG_ADDR_SIZE 2

uint8_t eeprom_write(uint16_t addr, uint8_t *data, uint16_t count);
uint8_t eeprom_read(uint16_t addr, uint8_t *data, uint16_t count);

#endif
